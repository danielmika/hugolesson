FROM python:3.6-buster
RUN pip install flask
COPY server.py .
CMD [ "/bin/sh", "-c", "FLASK_APP=server.py flask run --host=0.0.0.0" ]
